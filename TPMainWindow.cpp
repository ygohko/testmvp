/*
 * Copyright (c) 2020 Yasuaki Gohko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE ABOVE LISTED COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "TPMainWindow.h"
#include "ui_TPMainWindow.h"

// TestMVP includes
#include "TPMainWindowPresenter.h"

TPMainWindow::TPMainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::TPMainWindow) {
    ui->setupUi(this);
    this->presenter = new TPMainWindowPresenter();
    this->presenter->setView(this);
}

TPMainWindow::~TPMainWindow() {
    delete this->presenter;
    delete ui;
}

void TPMainWindow::calculate() {
    auto price = ui->priceLineEdit->text().toInt();
    this->presenter->calculate(price);
}

void TPMainWindow::updateTax(int tax) {
    ui->taxLineEdit->setText(QString("%1").arg(tax));
}

void TPMainWindow::updateTotalPrice(int totalPrice) {
    ui->totalPriceLineEdit->setText(QString("%1").arg(totalPrice));
}
