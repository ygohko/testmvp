/*
 * Copyright (c) 2020 Yasuaki Gohko
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE ABOVE LISTED COPYRIGHT HOLDER(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "TPMainWindowPresenter.h"

// std includes
#include <memory>

// TestMVP includes
#include "TPCalculator.h"
#include "TPMainWindowView.h"

typedef std::unique_ptr<TPCalculator> TPCalculatorPointer;

TPMainWindowPresenter::TPMainWindowPresenter() {
    this->view = nullptr;
}

TPMainWindowPresenter::~TPMainWindowPresenter() {
    // Do nothing
}

void TPMainWindowPresenter::calculate(int price) {
    auto calculator = TPCalculatorPointer(new TPCalculator());
    auto tax = 0;
    auto totalPrice = calculator->execute(price, &tax);
    this->view->updateTax(tax);
    this->view->updateTotalPrice(totalPrice);
}

void TPMainWindowPresenter::setView(TPMainWindowView* view) {
    this->view = view;
}
